let _ = require('lodash');
let DateTime = require('node-datetime');
let Poloniex = require('poloniex-api-node');

let DynamoDB = require('aws-sdk/clients/dynamodb');
let Sns = require('aws-sdk/clients/sns');

let poloniex = new Poloniex();
let dynamodb = new DynamoDB({
  region: 'us-west-1'
});

// SMS via SNS works only in certain regions.
let sns = new Sns({
  region: 'us-west-2'
});




// Ripple
poloniex.subscribe('USDT_XRP');
poloniex.subscribe('BTC_XRP');

// Ethereum
poloniex.subscribe('USDT_ETH');
poloniex.subscribe('BTC_ETH');

// Bitcoin
poloniex.subscribe('USDT_BTC');


poloniex.on('message', (channelName, data, seq) => {

  _.forEach(data, function(trade) {
    if (trade.type == 'newTrade') {

      let exchange = "poloniex";
      let timestamp = DateTime.create(trade.data.date).getTime().toString();
      let uniqueId = exchange + '-' + channelName + '-' + trade.data.tradeID;

      let data = {
        TableName: 'aitrader_trades_history',
        Item: {
          'uniqueId': {
            S: uniqueId
          },
          'timestamp': {
            N: timestamp
          },

          'currency': {
            S: channelName
          },
          'rate': {
            N: trade.data.rate
          },
          'amount': {
            N: trade.data.amount
          },
          'type': {
            S: trade.data.type
          },
          'exchange': {
            S: exchange
          },
        }
      };

      console.log(exchange + ' | ' + channelName + ' | ' + timestamp + ' | ' + trade.data.rate + ' | ' + trade.data.amount + ' | ' + trade.data.type);

      dynamodb.putItem(data, function(error, data) {
        if (error) {
          sendErrorSms(error);
        }
      })
    }
  });

});

poloniex.on('open', () => {
  console.log('Poloniex WebSocket connection open');
});

poloniex.on('close', (reason, details) => {
  sendErrorSms('Poloniex WebSocket connection disconnected: '+ reason);
  poloniex.openWebSocket({
    version: 2
  });
});

poloniex.on('error', (error) => {
  sendErrorSms(error);
});

poloniex.openWebSocket({
  version: 2
});

function sendErrorSms(message)
{
  console.error(message);

  let sns_data = {
    Message: message,
    MessageAttributes: {
      'AWS.SNS.SMS.SenderID': {
        DataType: 'String',
        StringValue: 'AiTrader'
      },
      'AWS.SNS.SMS.SMSType': {
        DataType: 'String',
        StringValue: 'Transactional'
      }
    },
    PhoneNumber: '',
  }

  sns.publish(sns_data, function(error, data) {
    if (error) {
      console.error(error);
    }
  });

}
