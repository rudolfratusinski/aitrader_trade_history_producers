# README #

## DynamoDB:
Table name: 		aitrader_trades_history
Partition key: 	S:	currency (BTC_ETH, BTC_USDT, XRP_ETH etc...)
Sort key: 		N:	timestamp (miliseconds)

## EC2:

AMI: Amazon Linux 2


```sh
#!/bin/bash

# Installing packages
yum update -y
curl --silent --location https://rpm.nodesource.com/setup_9.x | bash -
yum install git nodejs -y

# Cloning repository
cd /home/ec2-user/
git clone https://bitbucket.org/rudolfratusinski/aitrader_trade_history_producers

# Installing node.js packages
cd aitrader_trade_history_producers

# Using unsafe-perm because of npm issues with some packages when installing as root.
npm install aws-sdk node-datetime poloniex-api-node lodash --unsafe-perm

# Changing user to default non-root, because of security reasons
chown ec2-user:ec2-user . -R

# Installing and starting service
cp services/aitrader-poloniex.service /lib/systemd/system/
systemctl daemon-reload
systemctl start aitrader-poloniex
```